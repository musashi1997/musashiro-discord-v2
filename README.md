# Musashiro discord bot

<a href="https://gitlab.com/musashi1997/musashiro-discord-v2">
    <img src="./assets/images/nep.jpg" alt="Avatar" width="200" height="170">
</a>

A multi-purpose fun discord bot

## Description

Main objective of this porject is utilizing priodic tasks to change voice-channel names to the most game played by member of the said voice-channel.

example:
```Casual III -> Casual III (Valorant)```

it will do the said task by checking each channel and members inside and reading the <b>"Playing"</b> activity name then counting and putting the most played (not less than 2) as the new channel name inside parentheses.

### Commands

#### Working commands

* ```$random``` - gets a random image from reddit (more subreddits can be added in the python dictionary inside source code) and sends it as an URL

* ```$activity``` - changes name of the senders channel depending on the activity of members (at least two members should be "Playing" the same activity)

* ```$boom``` - plays a soundtrack and disconnects everyone in the same voice channel as the sender

* ```$nobtc``` - plays a sadening inquiry and leaves (testing voice  functionality)

* ```$silence``` - joins and plays soundtrack and mutes everyone temperorily(5s) to maintain order :smile:

* ```$flashbang``` - plays flashbang sound and scatters members to other channels randomly

#### Under work commands

* ```$ostale```

## Getting started

* Install dependencies from the req file.
* Uncomment first 4 lines and add a .env file to root of project containing static discord token and reddit api strings to test and debug without Docker

example of .env file:

```.env
DISCORD_CLIENT_TOKEN=<token goes here>
REDDIT_USER_AGENT=<reddit user goes here>
REDDIT_CLIENT_SECRET=<secret goes here>
REDDIT_CLIENT_ID=<reddit client id goes here>
```

Note: Don't forget to comment the latter global vars which are getting auth strings from docker container.

### Docker

Docker is supported feed discord and reddit auth strings as --build-arg when building the docker image with the ```Dockerfile```.
2 docker build arguments are required to pass the auth data for discord and reddit API

* ```--build-arg DCT=${a}``` DCT is the discord static token
* ```--build-arg GEN=${b}``` GEN is reddit con strings formatted as ```<REDDIT_USER_AGENT>*<REDDIT_CLIENT_SECRET>*<REDDIT_CLIENT_ID>```
which can be passed at CI pipeline as variables. see ```.gitlab-ci.yml``` file

Feel free to mount the file.log at /app/file.log out of container to have presistent logged data.

## License

Distributed under the MIT License. See `LICENSE` for more information.
