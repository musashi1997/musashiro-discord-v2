from cmath import log
from multiprocessing.connection import wait
import discord
import praw
import random
from decouple import config
from asyncio import sleep
from discord.ext import tasks
from prawcore.exceptions import NotFound
import logging
import time
import os
import json
from discord.errors import Forbidden

# Uncomment lines blow for local .env file in root of application
# DISCORD_CLIENT_TOKEN = config("DISCORD_CLIENT_TOKEN")
# REDDIT_USER_AGENT = config("REDDIT_USER_AGENT")
# REDDIT_CLIENT_SECRET = config("REDDIT_CLIENT_SECRET")
# REDDIT_CLIENT_ID = config("REDDIT_CLIENT_ID")


DISCORD_CLIENT_TOKEN = os.environ["DISCORD_CLIENT_TOKEN"]
GENERAL = os.environ["GENERAL"].split("*")
REDDIT_USER_AGENT = GENERAL[0].replace("_", " ")
REDDIT_CLIENT_SECRET = GENERAL[1]
REDDIT_CLIENT_ID = GENERAL[2]

logging.basicConfig(
    filename="./logs/file.log", format="%(asctime)s - %(message)s", level=logging.INFO
)


intents = discord.Intents.default()
intents.members = True
intents.message_content = True
intents.presences = True
client = discord.Client(intents=intents)
r = praw.Reddit(
    client_id=REDDIT_CLIENT_ID,
    client_secret=REDDIT_CLIENT_SECRET,
    user_agent=REDDIT_USER_AGENT,
)
number_of_images = 5

# surely you can use your own wicked sub reddits :)

randomSubs = {"funny": 1.0, "pics": 0.1}

with open("subs.json") as json_file:
    randomSubsNSFW = json.load(json_file)


def chooseSubReddit():
    keys = list(randomSubs.keys())
    key = random.choice(keys)
    value = randomSubs[key]
    while random.random() > value:
        key = random.choice(keys)
        value = randomSubs[key]
    return key


def chooseSubRedditNSFW():
    keys = list(randomSubsNSFW.keys())
    key = random.choice(keys)
    value = randomSubsNSFW[key]
    while random.random() > value:
        key = random.choice(keys)
        value = randomSubsNSFW[key]
    return key


def logActivity(user, command):
    logging.info(f"{user} has initiated {command}")


@client.event
async def on_ready():
    print(f"We have logged in as {client.user}")
    my_background_task.start()


@tasks.loop(seconds=120)  # task runs every 120 seconds
async def my_background_task():
    print("starting Task")
    channs = []
    for guild in client.guilds:
        channs = []
        channs.clear()
        # if guild.name.startswith("Potato"):  # change to your own server name
        for channel in guild.voice_channels:
            channs.append(channel)
        for c in channs:
            acts = []
            acts.clear()
            my_dict = {}
            my_dict.clear()
            for m in c.members:
                if m.activity is not None and m is not None:
                    logging.info(f"{m.name} in {c.name} doin {m.activity.name}")
                    if m.activity.type is discord.ActivityType.playing:
                        acts.append(m.activity.name)
            if len(acts) > 0:
                my_dict = {i: acts.count(i) for i in acts}
                new_value = max(my_dict, key=my_dict.get)
                if my_dict[new_value] >= 2:
                    cname = c.name.split("(")[0].strip()
                    try:
                        await c.edit(name=(cname + f" ({new_value})"))
                    except Forbidden:
                        continue
                    logging.info(f"changed channel name to: cname ({new_value})")

                else:
                    cname = c.name.split("(")[0].strip()
                    try:
                        await c.edit(name=(cname))
                    except Forbidden:
                        logging.warning(
                            f"insufficient prems to change channel {c.name} in server {c.guild.name}"
                        )
                        continue
                    logging.info(
                        "no more than 2 are playing the same shit " + str(my_dict)
                    )
            else:
                if "(" in c.name:
                    cname = c.name.split("(")[0].strip()
                    try:
                        await c.edit(name=(cname))
                    except Forbidden:
                        logging.warning(
                            f"insufficient prems to change channel {c.name} in server {c.guild.name}"
                        )
                        continue
                    logging.info(f"0 ppl in {cname}")


@client.event
async def on_message(message):
    if message.author == client.user:
        return
    # testing to get all channels of the currently connected user

    if message.content.startswith("$get"):
        logActivity(message.author.name, "$get")
        channs = []
        for guild in client.guilds:
            if guild == message.author.guild:
                for channel in guild.voice_channels:
                    print(channel)
                    channs.append(channel)

    if message.content.startswith("$random"):
        logActivity(message.author.name, "$random")
        print(message)
        subred = chooseSubReddit()
        randomimg = r.subreddit(subred).random()
        await message.channel.send(randomimg.url)

    if message.content.startswith("$nsfw"):
        logActivity(message.author.name, "$nsfw")
        notfound = True
        sublist = []
        sublist.clear()
        chosen = []
        chosen.clear()
        while notfound:
            subred = chooseSubRedditNSFW()
            try:
                hot = list(r.subreddit(subred).hot(limit=100))
            except NotFound:
                continue
            for h in hot:
                if h.is_reddit_media_domain or h.is_video:
                    sublist.append(h.url)
                if len(sublist) < 10:
                    continue
                chosen = random.sample(sublist, 10)
                notfound = False
            if len(chosen) < 1:
                notfound = True
                continue
            await message.channel.send(f"{subred}")
            for s in chosen:
                await message.channel.send(f"{s}")
        return

    if message.content.startswith("$flashbang"):
        logActivity(message.author.name, "$flashbang")
        voice_channel = message.author.voice.channel
        if voice_channel is None:
            await message.channel.send("Not in a voice channel")
            return
        message_guild = voice_channel.guild
        voice_channels = message_guild.voice_channels
        voice = discord.utils.get(client.voice_clients, guild=message.guild)
        if voice is not None:
            await voice.disconnect
        channel = voice_channel.name
        vc = await voice_channel.connect()
        # Don't forget to add ffmpeg location as PATH
        vc.play(
            discord.FFmpegPCMAudio(
                source="./assets/sounds/candella.m4a",
            )
        )
        # Sleep while audio is playing.
        while vc.is_playing():
            sleep(0.1)
        await vc.disconnect()
        for m in voice_channel.members:
            target_channel = random.choice(voice_channels)
            await m.move_to(target_channel)

    if message.content.startswith("$nobtc"):
        logActivity(message.author.name, "$nobtc")
        voice_channel = message.author.voice.channel
        channel = None
        if voice_channel is not None:
            voice = discord.utils.get(client.voice_clients, guild=message.guild)
            if voice is not None:
                voice.disconnect
            channel = voice_channel.name
            vc = await voice_channel.connect()
            # Don't forget to add ffmpeg location as PATH
            vc.play(
                discord.FFmpegPCMAudio(
                    source="./assets/sounds/No bitchis.m4a",
                )
            )
            # Sleep while audio is playing.
            while vc.is_playing():
                sleep(0.1)
            await vc.disconnect()
        else:
            await message.channel.send("sender not in a vc")

    if message.content.startswith("$silence"):
        logActivity(message.author.name, "$silence")
        voice_channel = message.author.voice.channel
        channel = None
        if voice_channel is not None:
            voice = discord.utils.get(client.voice_clients, guild=message.guild)
            if voice is not None:
                await voice.disconnect
            channel = voice_channel.name
            vc = await voice_channel.connect()
            vc.play(
                discord.FFmpegPCMAudio(
                    source="./assets/sounds/sokoot.mp3",
                )
            )
            while vc.is_playing():
                sleep(0.1)
            await vc.disconnect()
            for m in voice_channel.members:
                if m.name == "musashiro":
                    continue
                await m.edit(mute=True)
            time.sleep(5)
            for m in voice_channel.members:
                if m.name == "musashiro":
                    continue
                await m.edit(mute=False)

    if message.content.startswith("$boom"):
        logActivity(message.author.name, "$boom")
        voice_channel = message.author.voice.channel
        channel = None
        if voice_channel is not None:
            voice = discord.utils.get(client.voice_clients, guild=message.guild)
            if voice is not None:
                await voice.disconnect
            channel = voice_channel.name
            vc = await voice_channel.connect()
            vc.play(
                discord.FFmpegPCMAudio(
                    source="./assets/sounds/allahuakbar.m4a",
                )
            )
            while vc.is_playing():
                sleep(0.1)
            await vc.disconnect()
            channel = message.author.voice.channel
            for m in channel.members:
                await m.move_to(None)

    if message.content.startswith("$help"):
        logActivity(message.author.name, "$help")
        await message.channel.send(file=discord.File("./assets/images/help.PNG"))

    if message.content.startswith("$ostale"):
        logActivity(message.author.name, "$nobtc")
        voice_channel = message.author.voice.channel
        channel = None
        if voice_channel is not None:
            voice = discord.utils.get(client.voice_clients, guild=message.guild)
            if voice is not None:
                voice.disconnect
            channel = voice_channel.name
            vc = await voice_channel.connect()
            # Don't forget to add ffmpeg location as PATH
            vc.play(
                discord.FFmpegPCMAudio(
                    source="./assets/sounds/ostale.mp3",
                )
            )
            # Sleep while audio is playing.
            while vc.is_playing():
                sleep(0.1)
            await vc.disconnect()
        else:
            await message.channel.send("sender not in a vc")


client.run(DISCORD_CLIENT_TOKEN)
